function transform (rawflag) {
  var flag = rawflag.replace(/[A-Z]/g, function myFunction(x){
    return '-'+x;
  }).replace(/ +/g, '-').toLowerCase();
  // if (this[rawflag] instanceof String) {
  //   flag = this[rawflag]
  // } else 
  if (this[rawflag] instanceof String) {
    flag = `${flag}=${this[rawflag]}`;
  } else if (this[rawflag] === false ) {
    flag = `no-${flag}`;
  } else if (this[rawflag] !== true ) {
    flag = `${flag}=${this[rawflag]}`;
  }
  return rawflag.length === 1 ? `-${flag}` : `--${flag}`;
}
exports.default = function(options = {}) {
  return Object.keys(options).map(transform.bind(options));
}
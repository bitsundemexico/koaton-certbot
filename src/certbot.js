const util = require('util');
const writeFile = util.promisify(require('fs').writeFile);
const exec = util.promisify(require('child_process').exec);
const toFlags = require('./to-flags').default;
const {join} = require('path');
let cmd = 'certbot';
let scripts = join.bind(join, __dirname, '..', 'scripts');
async function command(subcommand, args = {}, options, callback) {
  if (args.manualAuthHook && args.manualCleanupHook) {
    await Promise.all([
      writeFile(scripts('auth.sh'), `#!/bin/bash
PHASE=validate CERTBOT_DOMAIN=$CERTBOT_DOMAIN CERTBOT_VALIDATION=$CERTBOT_VALIDATION CERTBOT_TOKEN=$CERTBOT_TOKEN challenge="${args.preferredChallenges}" node ${join(process.cwd(),args.manualAuthHook)}`, 'utf-8'),
      writeFile(scripts('clean.sh'), `#!/bin/bash
PHASE=clean CERTBOT_DOMAIN=$CERTBOT_DOMAIN CERTBOT_VALIDATION=$CERTBOT_VALIDATION CERTBOT_TOKEN=$CERTBOT_TOKEN challenge="${args.preferredChallenges}" node ${join(process.cwd(),args.manualCleanupHook)}`, 'utf-8')
    ]);
    await exec('chmod +x ./scripts/*');
    args.manualAuthHook = scripts('auth.sh');
    args.manualCleanupHook = scripts('clean.sh')
  }
  // console.log(await exec(args.manualAuthHook, options));
  const { stdout, stderr } = await exec(`${cmd} ${subcommand} ${toFlags(args).join(' ')}`, options);
  if (stderr) {
    throw err.toString();
  }
  return stdout.toString();
}
// async function command(subcommand, options = {}){
//   subcommand()
// }

function populate(target, commands) {
  for(const cmd of commands) {
    target[cmd] = command.bind(null, cmd);
  }
}
exports.default ={};
let API = ['certonly', 'renew']
populate(exports.default, API);
populate(module.exports, API);
exports.setpath = function(newpath) {
  cmd = newpath;
}
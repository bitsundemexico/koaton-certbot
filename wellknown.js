const { CERTBOT_TOKEN, CERTBOT_DOMAIN, CERTBOT_VALIDATION, CERTBOT_AUTH_OUTPUT, PHASE, challenge } = process.env;
let {writeFileSync, mkdirSync, unlinkSync} = require('fs');
const utils = require('util');
const { join } = require('path');
const { execSync } = require('child_process');

let challenges = {
  dns: {
    async validate () {
      const namecheap = require('namecheap-api').default;
      let { hosts } = await namecheap.domains.dns.getHosts(CERTBOT_DOMAIN);
      hosts.push(['TXT', '_acme-challenge', CERTBOT_VALIDATION]);
      console.log(await namecheap.domains.dns.setHosts(CERTBOT_DOMAIN, hosts));
    },
    async clean () {
      const namecheap = require('namecheap-api').default;
      let { hosts } = await namecheap.domains.dns.getHosts(CERTBOT_DOMAIN);
      let h = hosts.filter(h => h.name !== '_acme-challenge');
      console.log(await namecheap.domains.dns.setHosts(CERTBOT_DOMAIN, h));
    }
  },
  http: {
    async validate() {
      execSync(`mkdir -p ${join('/var', 'www', 'html', '.well-known', 'acme-challenge')}`);
      await writeFileSync(join('/var', 'www', 'html', '.well-known', 'acme-challenge', CERTBOT_TOKEN), CERTBOT_VALIDATION);
      await writeFileSync(join('/etc', 'nginx', 'sites-enabled', 'validations.conf'), 
      `server {
       listen 80;
       server_name ${CERTBOT_DOMAIN};
}`);
      console.log(execSync('service nginx restart').toString());
    },
    async clean () {
      unlinkSync(join('/etc', 'nginx', 'sites-enabled', 'validations.conf'));
      unlinkSync(join('/var', 'www', 'html', '.well-known', 'acme-challenge', CERTBOT_TOKEN));
      console.log(execSync('service nginx restart').toString());
    }
  }
};
challenges[challenge][PHASE]();

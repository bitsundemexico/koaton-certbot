const { CERTBOT_TOKEN, CERTBOT_DOMAIN, CERTBOT_VALIDATION, CERTBOT_AUTH_OUTPUT, PHASE } = process.env;
const namecheap = require('namecheap-api').default;
//console.log(CERTBOT_TOKEN);
(async function() {
  let {
    hosts
  } = await namecheap.domains.dns.getHosts(CERTBOT_DOMAIN);
  switch (PHASE) {
    case 'validate':
      hosts.push(['TXT', '_acme-challenge', CERTBOT_VALIDATION]);
      console.log(await namecheap.domains.dns.setHosts(CERTBOT_DOMAIN, hosts));
      break;
    case 'clean':
      let h = hosts.filter(h => h.name !== '_acme-challenge');
      console.log(await namecheap.domains.dns.setHosts(CERTBOT_DOMAIN, h));
      break;
  }
})();
const toFlags = require('../src/to-flags').default;
// console.log(toFlags({
// 	foo: 'bar',
// 	abc: true,
// 	xyz: 10,
// 	one: false,
// 	holaMundo: true,
// 	holaMundoNuevo: false,
// 	holaMundoMio: false,
// 	foo: 'bar',
// 	abc: true,
// 	xyz: 10,
// 	one: false,
// 	d: ['bitsun.com.mx', 'zpacioinmobiliario.com'],
// 	'a b c': true
// }));
const certbot = require('../src/certbot');
// console.log(certbot.certonly({
//   manual: true,
//   preferredChallenges: 'dns',
//   manualAuthHook: './namecheap.js',
//   manualCleanupHook: './namecheap.js',
//   d: ['zpacioinmobiliario.com'],
//   manualPublicIpLoggingOk: true
// }));

certbot.certonly({
  manual: true,
  preferredChallenges: 'http',
  manualAuthHook: './wellknown.js',
  manualCleanupHook: './wellknown.js',
  d: ['bitsun.com.mx','www.bitsun.com.mx'],
  manualPublicIpLoggingOk: true
});